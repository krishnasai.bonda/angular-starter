import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CounterComponent } from './counter/counter.component';
import { TodoComponent } from './todo/todo.component';
import { TodosComponent } from './todos/todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoService } from './todo.service';
import { FormsModule } from '@angular/forms';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { SignInFormComponent } from './sign-in-form/sign-in-form.component';
import { ValidatorComponent } from './validator/validator.component';
import { EqualValidator } from './equal-validator';
import { Routes, RouterModule } from '@angular/router';
import { HelloComponent } from './hello/hello.component';

export const ROUTES: Routes = [
  { path: 'counter', component: CounterComponent},
  { path: 'todos', component: TodosComponent},
  { path: 'sign-in-form', component: SignInFormComponent},
  { path: 'sign-up-form', component: SignUpFormComponent},
  { path: 'validator', component: ValidatorComponent},
  { path: 'hello', component: HelloComponent}

];
@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    TodoComponent,
    TodosComponent,
    TodoFormComponent,
    TodoListComponent,
    SignUpFormComponent,
    SignInFormComponent,
    ValidatorComponent,
   // ExampleComponentComponent
   EqualValidator,
   HelloComponent
 ],
  imports: [
    BrowserModule, FormsModule,  RouterModule.forRoot(ROUTES)
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }


