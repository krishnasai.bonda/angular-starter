import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent  {

  label: string;
  @Input() isEdit: boolean;
  @Input() todo: any;
  @Output() onAdd = new EventEmitter();
  @Output() onSave = new EventEmitter();
  @Output() onEditClick = new EventEmitter();

  save(title, id) {
    this.onSave.emit({title: title, id: id});

  }
  submit() {
    if (!this.label) {
      return;
    }
    this.onAdd.emit({label: this.label});
    this.label = '';

  }
}
