import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: any[];
  isEdit = false;
  todo: any;
  constructor(private todoService: TodoService) {}
  ngOnInit() {
    this.todos = this.todoService.getTodos();
  }
  onEditClick(id) {
    this.isEdit = true;
    this.todo = this.todos.filter(todo => id === todo.id)[0];
    console.log(this.todo);
  }
  addTodo({label}) {
    this.todos = [{label, id: this.todos.length + 1}, ...this.todos];
  }

  completeTodo({todo}) {
    this.todos = this.todos.map(
      item => item.id === todo.id ? Object.assign({}, item, {complete: true}) : item
    );
  }
  saveTodo(todo) {
   this.todos = this.todos.map((item) => { if (item.id === todo.id) {return { label: todo.title, ...item }; } else  {return item; } });

  }
  removeTodo({todo}) {
    this.todos = this.todos.filter(({id}) => id !== todo.id);
  }
}
